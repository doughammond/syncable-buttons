define([
	'Dexie',
	'Dexie.Observable',
	'Dexie.Syncable'
], function(Dexie, DexieObservable, DexieSyncable) {
	"use strict";

	var $content = $('#content');

	var delete_button = function(k) {
		getDb().prefs.delete(k).then(() => {
			$('#'+k, $content).remove();
		}).catch((err) => {
			console.log('put err', err);
		});
	};

	var create_button = function(k, init_state) {
		var $btn = $('#'+k, $content);
		if (!$btn.length) {
			$btn = $(
				'<div id="' + k  +'" class="btn-group" data-toggle="buttons">' +
				' <label class="btn btn-primary btn-xs">' +
				'  <input type="checkbox">' + k +
				' </label>' +
				' </div>'
			);
			$content.append($btn);

			$($btn).on('click', function(evt) {
				// console.log('btn click', evt, $btn);
				var v = !$('input', $btn).prop('checked');
				getDb().prefs.where('k').equals(k).modify({
					'v': v ? '1' : '0'
				}).catch((err) => {
					console.log('modify err', err);
				});
				getDb().prefs.put({
					'k': k,
					'v': v ? '1' : '0'
				}).catch((err) => {
					console.log('put err', err);
				});
			});
			console.log('created button', $btn);
		}

		getDb().prefs.where('k').equals(k).count().then((exists) => {
			if (!exists) {
				getDb().prefs.put({
					'k': k,
					'v': init_state ? '1' : '0'
				}).catch((err) => {
					console.log('put err', err);
				});
			}

			getDb().prefs.get(k).then((pref) => {
				set_button_state($btn, pref.v);
			});
		});

		return $btn;
	};

	var set_button_state = function($btn, v) {
		if (v == '1') {
			$('label', $btn).addClass('active');
			$('input', $btn).prop('checked', 'checked');
		} else {
			$('label', $btn).removeClass('active');
			$('input', $btn).prop('checked', '');
		}
	};

	var StatusMap = {
		"-1": ['label label-danger',  'Error'],
		"0":  ['label label-danger',  'Offline'],
		"1":  ['label label-warning', 'Connecting'],
		"2":  ['label label-success', 'Online'],
		"3":  ['label label-info',    'Syncing'],
		"4":  ['label label-warning', 'Waiting to sync']
	};

	var syncStatusTimeout = null;

	var on_sync_status_change = function (newStatus, url) {
		// console.log("Sync Status changed: ", newStatus, DexieSyncable.StatusTexts[newStatus]);
		var update = function () {
			$('#status').attr('class', StatusMap[newStatus][0]);
			$('#status').text(StatusMap[newStatus][1]);
		};

		if (newStatus == 3) {
			// only show syncing status if it lasts longer than 500ms
			syncStatusTimeout = setTimeout(update, 500);
		} else {
			clearTimeout(syncStatusTimeout);
			update();
		}
	};

	var on_prefs_changes = function(changes) {
		changes.forEach((change) => {
			// console.log(change);
			switch (change.type) {
				case 1: // create
				case 2: // update
					var $btn = create_button(change.key);
					var pref = change.mods || change.obj;
					set_button_state($btn, pref.v);
					break;
				case 3: // delete
					$('#'+change.key, $content).remove();
					break;
			}
		});
	};

	var register_sync_protocols = function() {
		var PrefsSyncProto = {
			'sync': function(
				context, url, options,
				baseRevision, syncedRevision,
				changes, partial,
				applyRemoteChanges,
				onChangesAccepted, onSuccess, onError
			) {
				/// <param name="context" type="IPersistedContext"></param>
				/// <param name="url" type="String"></param>
				/// <param name="changes" type="Array" elementType="IDatabaseChange"></param>
				/// <param name="applyRemoteChanges" value="function (changes, lastRevision, partial, clear) {}"></param>
				/// <param name="onSuccess" value="function (continuation) {}"></param>
				/// <param name="onError" value="function (error, again) {}"></param>

				var request_data = {
					'clientIdentity': context.clientIdentity || null,
					'baseRevision': baseRevision,
					'partial': partial,
					'changes': changes,
					'syncedRevision': syncedRevision
				};

				$.ajax(url, {
					'type': 'post',
					'dataType': 'json',
					'data': {
						'syncdata': JSON.stringify(request_data)
					},
					'error': (xhr, textStatus) => {
						onError(textStatus, options.interval);
					},
					'success': (data) => {
						if (!data.success) {
							onError(data.errorMessage, Infinity);
						} else {
							onChangesAccepted();
							applyRemoteChanges(
								data.changes,
								data.currentRevision,
								false,
								data.needsResync
							);
							onSuccess({'again': options.interval});
							if ('clientIdentity' in data) {
								context.clientIdentity = data.clientIdentity;
								context.save();
							}
						}
					}
				});
			}
		};
		DexieSyncable.registerSyncProtocol('prefs', PrefsSyncProto);
		console.log('registered protocol');
	};

	var _db = null;

	var getDb = function() {
		return _db;
	};

	var start = function() {
		var db = new Dexie(
			'test-db',
			{
				'addons': [DexieObservable, DexieSyncable]
			}
		);

		db.version(1).stores({
			'prefs': '&k,v'
		});

		register_sync_protocols();

		db.open().then((db) => {
			_db = db;

			db.syncable.on('statusChanged', on_sync_status_change);

			db.on('changes', (changes) => {
				on_prefs_changes(
					changes.filter(change => change.table == 'prefs')
				);
			});

			db.prefs.toArray().then((prefs) => {
				prefs.forEach((pref) => create_button(pref.k));
			});

			db.syncable.connect('prefs', '/prefs', { 'interval': 500 });
			console.log('connected');
		}).catch((err) => {
			console.log('err?', err);
		});
	};

	return {
		'start': start,
		'create_button': create_button,
		'delete_button': delete_button
	};
});
