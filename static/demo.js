requirejs.config({
	baseUrl: 'static',
	paths: {
		'text':             window.paths.cdn + 'text/text',
		'bootstrap':        window.paths.cdn + 'bootstrap/dist/js/bootstrap.min',
		'jquery':           window.paths.cdn + 'jquery/dist/jquery.min',
		'Dexie':            window.paths.cdn + 'dexie/dist/latest/Dexie.min',
		'Dexie.Observable': window.paths.cdn + 'dexie/addons/Dexie.Observable/Dexie.Observable',
		'Dexie.Syncable':   window.paths.cdn + 'dexie/addons/Dexie.Syncable/Dexie.Syncable',
	},
	shim: {
		'bootstrap': ['jquery'],
		'Dexie.Observable': ['Dexie'],
		'Dexie.Syncable': ['Dexie.Observable']
	}
});

requirejs([
	'jquery',
	'bootstrap',
	'main',
	],
	function($, _, test) {
		test.start();
	}
);