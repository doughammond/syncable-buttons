import json
import datetime
from pprint import pprint as pp
import flask
import peewee as pw

# --------- simple data stores --------------

db = pw.SqliteDatabase('./store.sqlite', threadlocals=True)

class Revision(pw.Model):
	class Meta(object):
		database = db
	revNo = pw.PrimaryKeyField(primary_key=True)
	user = pw.CharField()
	ts = pw.DateTimeField(default=datetime.datetime.utcnow)

class Pref(pw.Model):
	class Meta(object):
		database = db
		indexes = (
			# compound unique key
			(('key', 'revNo'), True),
		)

	key = pw.CharField()
	value = pw.TextField()
	revNo = pw.ForeignKeyField(Revision)
	deleted = pw.BooleanField(default=False)

# --------------- web app -------------------

app = flask.Flask(__name__)

@app.route('/')
def main():
	return flask.render_template('index.html')

@app.route('/cdn/<path:filename>')
def cdn(filename=None):
	return flask.send_from_directory('bower_components', filename)

class SyncTypes(object):
	CREATE = 1
	UPDATE = 2
	DELETE = 3

@app.route('/prefs', methods=['POST'])
def prefs():
	syncdata = json.loads(flask.request.form['syncdata'])
	pp(syncdata)

	maxRev = Revision.select().order_by(Revision.revNo.desc()).limit(1)
	if not maxRev.count():
		maxRev = Revision.insert(user='me').execute()
	else:
		maxRev = maxRev.first().revNo

	response = {
		'success': True,
		'changes': [],
		'currentRevision': maxRev,
		'needsResync': False
	}

	if syncdata['syncedRevision']:
		select = Pref.select().where(Pref.revNo > syncdata['syncedRevision'])
	else:
		select = Pref.select()

	for pref in select:
		response['changes'].append({
			'obj': {
				'k': pref.key,
				'v': pref.value
			},
			'mods': {
				'v': pref.value
			},
			'key': pref.key,
			'table': 'prefs',
			'type': SyncTypes.DELETE if pref.deleted else SyncTypes.CREATE
		})

	for item in syncdata['changes']:
		pp(item)
		pref = None
		newRev = Revision.insert(user='me').execute()
		if item['type'] == SyncTypes.CREATE:
			pref = Pref(
				key=item['key'],
				value=item['obj']['v'],
				revNo=newRev,
				deleted=False
			)
		if item['type'] == SyncTypes.UPDATE:
			pref = Pref(
				key=item['key'],
				value=item['mods']['v'],
				revNo=newRev,
				deleted=False
			)
		if item['type'] == SyncTypes.DELETE:
			pref = Pref(
				key=item['key'],
				value='',
				revNo=newRev,
				deleted=True
			)
		if pref:
			pref.save()

	print('---')
	pp(response)
	return json.dumps(response)


if __name__ == '__main__':
	db.connect()
	db.create_tables([Revision, Pref], safe=True)

	app.debug=True
	app.run()
